package main

import (
	"fmt"
	"sync"
)

var counter = 0

// waitgroup
// sync multiple go routines
var wg = sync.WaitGroup{}

// routines
// main function in in go routine
// make sure to end goroutines to prevent memory leaks
// check for race conditions at compile time, go run -race goconcurrency.go, this will show what vars are inconsistant

// mutex
// mutiple go routines will run in fastest order, not gaurented to be in code order
// mutexes make sure only one routine as access object at a time
var m = sync.RWMutex{}

func main(){
	max := 10

	
	for i:=0;i<max;i++{
		// adding below funcs to wg
		wg.Add(2)

		// locking read of counter var
		m.RLock()
		go write()

		// locking write of counter var
		m.Lock()
		go increment()
	}

	// application will wait until wg is free
	wg.Wait()
}

func increment(){
	counter++

	// unlocking write
	m.Unlock()
	wg.Done()
}

func write(){
	fmt.Println("Inc:", counter)

	// unlocking read
	m.RUnlock()
	wg.Done()
}

// goroutine with anon func
// go func(msg string){
// 	fmt.Println(msg)
// }(msg) // param of anon func, use this so variables are consistant