# GO Routing Notes

### Gin

Gin is a light weight HTTP framework for Go. Use for lightweight applications.

### Using Gin

Create a Go module to define a server struct that has a store of type *db.Store and a route of type *gin.Engine object.

Create a constructor that passes in the store object and initialize the route with gin.Default(). Return the server.

### Implementing Routes

Implement the routes inside the contructor.

Each route function has the route address, middlewares and the handler as the params.

The middlewares and the handler should be implemented in another module.

### Validation

Validation for each route is done in the params struct in each handler.

Put validation in the tags of each param. Documentation here: https://pkg.go.dev/github.com/go-playground/validator?utm_source=godoc#hdr-Baked_In_Validators_and_Tags

### Starting the Server

Write a start function in the server module that takes a param
