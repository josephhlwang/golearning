package main

// naming conventions
// use camelCase, acronyms should be all caps: ServeHTTPS, single letter represents index

// enumerated constants
// iota is a counter for number of enumerated constants
// const( _=iota
// 	a
// 	b
// 	c
// 	d
// )

func main(){

	// variable creation
	// Stringly typed, variables with no value assignment defaults with value 0
	// declared variables must be used

	// var num1, num2 int
	// var num3, num4 = 3,4

	// variable block
	// declare mutiple variables at once

	// var(
	// 	num1 int = 1
	// 	num2 = 2
	// )
	// fmt.Println(num1,num2,)

	// variable creation + assignment, implicit typing

	// num := 9
	// fmt.Println(num)

	// constants
	// cannot redeclare constants with same name in the same level

	// const num = 7
	// fmt.Println(num)

	// casting
	// int to string casting by ascci
	// use "strconv" package to convert int to string

	// var i int = 42
	// var j = float32(i)
	// var k = strconv.Itoa(i)
	// var l = string(i)

	// fmt.Printf("val: %f, type: %T\n", j, j)
	// fmt.Printf("val: %s, type: %T\n", k, k)
	// fmt.Printf("val: %v, type: %T\n", l, l)

	// primitive types
	// you must cast variables to the exact same type to do math operations ex. int + int8 = error, int + int(int8) = good

	// n := 1 == 2
	// fmt.Println(n)

	// Use complex64 data type for complex numbers
	// real(n), imag(n) to get real and imaginary part of n
	// complex(n1,n2) = n1 + n2i for a complex number
	
	// fmt.Println(a,b,c,d)

	// arrays
	// (SLICE IS BETTER) complier must know array size at compile time
	// "..."" any size array for explicit declaration
	// copying an array creates a new copy of the array, TAKES MEMORY
	// use pointers for immutible arrays ex. a = &n

	// n := [...]int{3,4,5}

	// var m [4]int

	// fmt.Println(n,m)

	// slices
	// anything you can do with array, you can do with slice
	// slices are pointers unlike arrays

	// n := []int{3,4,5}
	// // newSlice := n[:]
	// // make(type, length, capacity)
	// n = make([]int, 3, 100)
	// n = append(n, 1)
	// n = append(n, []int{2,3,4,5}...)
	// length := len(n)
	// capacity := cap(n)

	// fmt.Println(n[4])
	// fmt.Printf("Capacity: %v, Length: %v\n",capacity, length)
	
	// maps
	// just like slices, maps are pointers
	// key -> val
	// can only use key type that can be tested for equality, eg. cannot be slice or channal
	
	// m := make(map[string]int)

	// m = map[string]int{
	// 	"one": 1,
	// 	"two": 2,
	// 	"three": 3,
	// }
	// m["four"] = 4
	// delete(m, "four")

	// // when accessing an non-existant element, Go will return 0, use var "ok" to test if element exists
	// testok, ok := m["lol"]

	// fmt.Println(testok, ok)
	// fmt.Println(m)
	
	// structs
	// essentially classes, but are not immunible
	// when you copy them, you create new copy
	// public = Captitalize, private = lowercase
	
	// type Person struct{
	// 	name string
	// 	Weight int
	// 	Companions []Person
	// }

	// jim := Person{name:"Jim", Weight:140}
	// tim := Person{name:"Tim", Weight:150, Companions: []Person{jim}}
	// bim := Person{"Bim", 130, []Person{tim}} 

	// implicit struct declaration
	// another way to initialize structs, not good
	// person := struct{name string}{name: "Joseph"}
	// cop := &jim
	// cop.name = "Jojo"

	// fmt.Println(jim)
	// fmt.Println(tim)
	// fmt.Println(cop)

	// embedding
	// this is different than interface
	// embedd for params

	// type Animal struct{
	// 	// tags are a string of text, can be used for validation
	// 	Name string `required max:"100"`
	// 	Origin string
	// }

	// type Bird struct{
	// 	Animal
	// 	Wingspan int
	// 	Canfly bool
	// }

	// emu := Bird{Animal: Animal{Name:"Emu", Origin:"Australia"}, Wingspan: 2, Canfly: false}
	// OR
	// emu.Name = "Emu"
	// emu.Origin = "Australia"
	// emu.Wingspan = 2
	// emu.Canfly = false

	// fmt.Println(emu)
	
	// conditionals
	// || or, && and, < lt, > gt, <= leq, >= geq, == eq, ! not
	// short circuiting in Go like Java, doesn't check rest of conditions after first true

	// num:=2
	// if num == 5{
	// 	fmt.Println("HI")
	// } else if num > 5 {
	// 	fmt.Println("NO")
	// } else {
	// 	fmt.Println("BYE")
	// }

	// m := map[string]int{
	// 	"zero": 0,
	// 	"one": 1,
	// 	"two": 2,
	// 	"three": 3,
	// }

	// embedded conditionals
	// if num, ok := m["zero"]; ok{
	// 	fmt.Println(num)
	// }

	// loops
	
	// breaks, continue
	// for i,j := 0,0; (i<10); i,j = i+1,j+2 {
	// 	if i == 4{
	// 		continue
	// 	}

	// 	if i == 5{
	// 		break
	// 	}
			
	// 	fmt.Println(i,j)
	// }

	// looping through arrays, slices and maps
	// s:= []int{1,2,3,4}

	// for key,val := range s{
	// 	fmt.Println(key,val)
	// }

	// defer

	// defering a function makes it run right before the return in that code block
	// deferring multiple function runs in Last In First Out order
	// deferring usually used to close resources(httpreq or file)
	// defer takes arguements at the time the defer is called

	// panic
	// similar to throwing an error 
	// panic happen after deffered  

	// panic("something bad happened")

	// recover
	// recover() will recover to the parent code block of the panic; if non exists, program will end
	
	// pointers
	// var b *int creates a pointer to an int
	// & is "address of" operator
	// we can work with pointers whereever we can use a variable

	// var a int = 42
	// var b *int = &a
	// a=40
	// *b = 60
	// deferencing a pointer
	// fmt.Println(a,*b)
	
	// unintialized pointers are initialized to nil, check if nil pointer function params
	
	// functions

	// hello("Hello", "Joseph")
	// fmt.Println(sum("LOL",1,2,3,4))
	
	// error handling
	// val, err:= div(5,0)

	// if err != nil{
	// 	fmt.Println(err)
	// 	return
	// }
	// fmt.Println(val)

	// anonomous function
	// good for call back functions
	
	//or var sum func(int,int)(int,error) = func(num1,num2 int) (int, error){.......
	// sum := func(num1,num2 int) (int, error){
	// 	fmt.Println("Hello")
	// 	return num1+num2,nil
	// }

	// sum(1,2)
	
	// methods

	// var greet Greeter = Greeter{"Hello world", 5}
	// greet.greet()
	// greet.greet()

	// interfaces
	// interface for methods

	// here the writer uses the method implemented by ConsoleWriter
	// var w Writer = ConsoleWriter{}

	// w.Write([]byte("Hello world!"))

	// 
	// var wc WriterCloser = ConsoleWriterCloser{}

}

// functions
// func hello(msg string, name string){
// 	fmt.Println(msg,name)
// }

// variatic parameters
// you can only have one, and it must be at the end
// func sum(note string,values ...int) (string, []int){
// 	return note, values
// }

// returning errors
// standard in go to return error

// func div(num1, num2 float32) (float32, error){
// 	if num2 == 0 {
// 		return 0,fmt.Errorf("Error!")
// 	}

// 	return num1/num2,nil
// }

// methods for structs(classes)
	
// type Greeter struct{
// 	greeting string
// 	times int
// }

// we get copy of struct, not struct itself
// func (G Greeter) greet(){
// 	for i:=0;i<G.times;i++{
// 		fmt.Println(G.greeting)
// 	}
// 	// this doesn't do anything
// 	// to make this work, make G Greeter a pointer => G *Greeter in params
// 	G.greeting="HOHOHO"
// }

// interfaces
// naming convension, if interface is single method, name <method name>+er, Eg. Writer 
// use many small interfaces, compose small interfaces for bigger interfaces
// don't export interfaces for types that will be consumed by others, jsut export the type
// do export interfaces comsumed by package

// type Writer interface{
// 	Write([]byte)(int, error)

// }

// type ConsoleWriter struct {
// }

// interfaces are not explicitly implemented
// to use the interface, create a method with the same name as the interface
// func (cw ConsoleWriter) Write(data []byte)(int,error){
// 	n,err := fmt.Println(string(data))
// 	return n,err
// }

// composing interfaces together, embedding interfaces
// implement both interfaces defined by the composed interface

// type Closer interface{
// 	Close() error

// }

// type WriterCloser interface{
// 	Writer
// 	Closer
// }

// type ConsoleWriterCloser struct {
// }

// func (cwc ConsoleWriterCloser) Close()(error){
// 	// close..
// }

// func (cwc ConsoleWriterCloser) Write()(error){
// 	// write..
// }