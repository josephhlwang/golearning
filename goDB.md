# PostgreSQL(Docker) Notes

SQL notes for: https://www.youtube.com/playlist?list=PLy_6D98if3ULEtXtNSY_2qN21VCKgoQAE

Table Plus: https://tableplus.com/

### Schema Creation

Use https://dbdiagram.io/ to design, visualize and generate SQL schema.

### DB Migration(Automated Initialization)

After initialization of Docker container of db, install Go migration library. Then create a new migration dir(db/migration) and initialize up and down scripts using SQL schema created earlier.
Use below command to automatically initialize database with schema using migration files.

###### Migrate Command

```sh
migrate -path <migration_files_path> -database <db_connection> -verbose <up || down>
```

### Implementing CRUD operations in Go

1. Program your own code using standard Go libs.

2. Use GORM, generate Go code form querys(Slow).

3. Use SQLC/SQLX, generate Go code form querys(Faster).

# SQLC

### YAML File

Generate settings file using

```sh
sqlc init
```

Inside the YAML file, change settings to match project.

```yaml
version: "1"
packages:
  - name: "db" #name of Go package to be generated
    path: "./db/sqlc" #path to generated package
    queries: "./db/query/" #path to queries
    schema: "./db/migration/" #path to schemas
    engine: "postgresql"
    emit_prepared_queries: true
    emit_interface: false
    emit_exact_table_names: false
    emit_empty_slices: false
    emit_json_tags: true
```

### Generating Go Code

In the specified query path, create SQL queries for your tables.
Once finished, run `sqlc generate` to generate Go code to run CRUD operations on your db in Go.

### Transactions

Transactions must satisfy the ACID principal:

1. Atomicity, either all operations comlete successfully or the transaction fails and db is unchanged.
2. Consistency, the db state must be valid after the transaction.
3. Isolation, concurrent transactions must not affect each other.
4. Durability, data written by a successful transaction must be recorded in persistent storage.

To implement, create _store_ module to implement each transaction function.
The _store_ module contains the query and db objects as well as any stucts needed for the transaction process.

### Avoiding Deadlock in Go

When doing concurrent database queries, make sure to set an order of changes during a transaction.

Ex. In the back-end code, check the values of id and order the transaction based on which id is smaller. This will help avoid deadlocks.
