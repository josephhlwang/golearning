package main

import (
	"fmt"
	"sync"
)

var wg = sync.WaitGroup{}

// channels
// channels are strongly typed
// "deadlocks" happen when routines have no exit condition

func main(){
	// must use make() to create channel
	// second param 10 create "buffer" to store incoming data when the receiver is slow
	// buffers prevent deadlocks from incomming data
	ch := make(chan int, 50)

	wg.Add(2)

	// receive only channel
	go func(ch <-chan int) {
		// received in channel

		// one way of looping through channels
		// for i:= range ch{
		// 	fmt.Println(i)
		// }

		// other way
		for {
			if i,ok := <- ch; ok{
				fmt.Println(i)
			}else{
				break
			}
		}

		wg.Done()
	}(ch)


	// send only channel 
	go func(ch chan<- int) {
		i:=42
		j:=6

		// passes copy of i,j into c
		ch <- i
		ch <- j

		// close channel when this application is completely done using channel
		close(ch)
		wg.Done()
	}(ch)

	wg.Wait()
}