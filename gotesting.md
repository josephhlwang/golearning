# GO Testing Notes

### Naming Conventions

Put test files in the same folder as code and use the same package.

To test the _accounts.go_ module, name the test file _accounts_test.go_.

Every unique test function in Go must start with the _Test_ prefix with uppercase _T_. Eg. *TestCreateAccount(t *testing.T)\*

### main_test.go

Running a test for a module starts in the _main_test.go_ file. In this file declare the main entry point of all testing functions for given module, named _TestMain(m \*testing.M)_.

### Running Tests in Go

Use command `go test -v -cover ./...` to test all units tests in all libs, show verbose and cover.

### Writing Tests(TDD)

Write unit tests before implementing code. Test code while implementing. You know you have strong code when all unit tests pass.

Each tests should be independent from the other. Use testify/require package to validate returned object from db.

### Testing Concurrency

Testing functions that have concurrency requires go routines and channels. Eg. db-transactions.

Write the test(s) inside go routines and pass the result and err into two separate channels. Validate the errors and results outside of the go routine.

### Testing for Deadlock in DB

You can test for deadlock in a DB by doing multiple concurrent queries from a single row.

### Testing HTTP API

Use GoMock to write tests to build stubs that return hard-coded values. Use these stubs to rest API functions.
